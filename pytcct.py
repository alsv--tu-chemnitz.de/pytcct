# Copyright (c) 2019-2021 Sven Albrecht
#
# This file is part of PyTCCT
# (see https://gitlab.hrz.tu-chemnitz.de/alsv--tu-chemnitz.de/pytcct).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# ---------------------------------------
# NOTES
# ---------------------------------------
# execute "export LANG=C; export LC_ALL=C" before running script to avoid locale error
# ---------------------------------------
# TODO
# ---------------------------------------
# [DONE] error handling, esp. for disconnects, i.e. try to reconnect
# FUTURE FEATURES
# * use db backend for saving data
# * save whole JSON Objects
# * handle/export data
# ---------------------------------------
import sys
import logging
import tweepy
import langid
import csv
import codecs
import configparser
from datetime import date

# Setup logging, c.f. https://docs.python.org/3/howto/logging-cookbook.html
logger = logging.getLogger("PyTCCT")
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler("pytcct.log")
fh.setLevel(logging.getLevelName(logging.DEBUG))
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.getLevelName(logging.INFO))
# create formatter and add it to the handlers
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

# read configuration from ini file
config = configparser.ConfigParser()
try:
    config.read("settings.ini")
    logger.debug("read settings.ini successfully")
except:
    logger.critical("file settings.ini missing")

try:
    consumer_key = config["Twitter"]["consumer_key"]
    consumer_secret = config["Twitter"]["consumer_secret"]
    access_key = config["Twitter"]["access_key"]
    access_secret = config["Twitter"]["access_secret"]
    logger.debug("read API keys successfully")
    # using language filtering is an optional feature
    try:
        tweet_language = config["language"]["tweet_language"]
        if tweet_language:
            logger.info("Using language filtering")
    except KeyError:
        logger.debug("Language filtering not defined in config file.")
    try:
        keyword_file = config["keywords"]["filename"]
        if keyword_file:
            logger.info("Using keyword filtering, filename: %s", keyword_file)
    except KeyError:
        logger.debug("Keyword file not defined in config file.")
    try:
        location_string = config["geolocation"]["bounding_box"]
        if location_string:
            logger.info("Using boundingbox: %s", location_string)
    except KeyError:
        logger.debug("Bounding Box not defined in config file.")
    try:
        log_level_file = config["internal"]["log_level_file"]
        logger.info("Log level of log file is: %s", log_level_file)
        fh.setLevel(logging.getLevelName(log_level_file))
        log_level_stdout = config["internal"]["log_level_stdout"]
        logger.info("Log level of stdout file is: %s", log_level_stdout)
        ch.setLevel(logging.getLevelName(log_level_stdout))
    except KeyError:
        logger.error('Could not read log level settings. Using default values.')
        #log_level_file = "DEBUG"
        #log_level_stdout = "INFO"
except:
    logger.critical("Unexpected error: %s", sys.exc_info()[0])

# authenticate app, c.f. https://docs.tweepy.org/en/latest/auth_tutorial.html
try:
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    api = tweepy.API(auth)
except:
    logger.critical("oauth2 failed")

# Create stream listen for twitter API
logger.debug("creating stream listener...")


class MyStreamListener(tweepy.StreamListener):
    # we take drastic measures to keep the stream alive, not sure if that's a good idea
    def on_status(self, status):
        #        date = date.today()
        logger.debug("opening tweets file for writing")
        # use codecs.open to support unicode for Asian languages etc.
        with codecs.open(
            "tweets_{}.txt".format(date.today()),
            mode="a",
            encoding="utf-8",
            buffering=-1,
        ) as output:
            csv_writer = csv.writer(
                output, quoting=csv.QUOTE_NONNUMERIC, lineterminator="\n"
            )
            try:
                # check if we are filtering by language
                # if tweet_language:
                if "tweet_language" in globals():
                    lang = langid.classify(status.text)[0]
                    # language matches
                    if lang == tweet_language:
                        logger.debug("tweet language matches filter language")
                        # note: it's important to remove new line characters from tweets to not break the CSV file
                        csv_writer.writerow(
                            (
                                status.created_at,
                                status.id_str,
                                status.text.replace("\n", " "),
                                status.geo,
                            )
                        )
                else:
                    # we' re not filtering by language
                    csv_writer.writerow(
                        (
                            status.created_at,
                            status.id_str,
                            status.text.replace("\n", " "),
                            status.geo,
                        )
                    )
            except:
                # catch and discard errors, this is usually due to iffy character encoding, maybe use UTF-32 to be sure
                logger.error("Caught error %s %s", sys.exc_info()[0], sys.exc_info()[1])
                sys.exc_clear()

    # redefine on_error behavior to prevent disconnects
    def on_error(self, status_code):
        if status_code == 420:
            # returning False in on_error disconnects the stream
            logger.error("hit rate limit, reconnect in 5 minutes")
            # we don't really need this, tweepy implements the official Twitter backoff time limits
            time.sleep(300)
            return True
        # all other errors
        logger.error("received error code: %s", status_code)
        return True
        # returning non-False reconnects the stream, with backoff.

    def on_timeout(self):
        # just try to reconnect
        logger.error("application timed out, reconnecting...")
        return True


logger.info("Starting stream")
mystream = tweepy.streaming.Stream(auth, MyStreamListener(), timeout=1000.0)

if "location_string" in globals() and "keyword_file" not in globals():
    # only location
    try:
        bblocations = [float(x) for x in location_string.split(",")]
    except:
        logger.error("Could not set bounding box.")
    logger.info("Filtering for location only.")
    mystream.filter(locations=bblocations, filter_level="none")
elif "keyword_file" in globals() and "location_string" not in globals():
    # only keywords
    logger.info("Filtering for keywords only.")
    try:
        terms = [line.strip() for line in codecs.open(keyword_file, mode="r")]
        logger.info("Using terms: %s", terms)
    except:
        logger.error("Could not load keywords.")
    # logger.info("Using terms: %s", terms)
    mystream.filter(track=terms, filter_level="low")
elif "keyword_file" in globals() and "location_string" in globals():
    # both set
    try:
        terms = [line.strip() for line in codecs.open(keyword_file, mode="r")]
        logger.info("Using terms: %s", terms)
    except:
        logger.error("Could not load keywords.")
    try:
        bblocations = [float(x) for x in location_string.split(",")]
    except:
        logger.error("Could not set bounding box.")
    logger.info('Both keyword and location filtering enabled.')
    logger.info('Note that this will result in tweets being collected that EITHER match the keywords OR the bounding box!')
    mystream.filter(track=terms, locations=bblocations, filter_level="none")
else:
    # none set
    logger.critical("Neither keywords nor bounding box defined. Exiting.")
