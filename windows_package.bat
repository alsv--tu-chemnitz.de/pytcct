@ECHO OFF
ECHO Creating standalone package of PyTCCT.
pyinstaller --add-data="settings.ini.example;." --add-data="keywords.txt;." --add-data="README.md;." --add-data="COPYING;." pytcct.py
ECHO Finihed.
PAUSE