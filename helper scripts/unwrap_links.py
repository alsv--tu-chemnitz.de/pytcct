# Copyright (c) 2019-2021 Sven Albrecht
#
# This file is part of PyTCCT
# (see https://gitlab.hrz.tu-chemnitz.de/alsv--tu-chemnitz.de/pytcct).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


# importing the required modules
import glob
import re
import os
import csv
import requests
import sys
from multiprocessing import Pool


def unwrap_url(url_string):
    try:
        full_url = requests.get(url_string).url
    except Exception:
        full_url = url_string
    return full_url


def get_delimiter(file_path, bytes=8192):
    sniffer = csv.Sniffer()
    data = open(file_path, "r", encoding="utf-8").read(bytes)
    delimiter = sniffer.sniff(data).delimiter
    return delimiter


def process_file(filename):
    # generate new filename for output
    name, extension = os.path.splitext(filename)
    new_filename = name + "_unwrapped" + extension
    # open your csv and read as a text string
    with open(filename, "r", encoding="utf-8") as f:
        print("Working on", filename)
        delimiter = get_delimiter(filename)
        print("Delimiter is", delimiter)
        csvReader = csv.reader(f, delimiter=delimiter)
        with open(new_filename, "w") as output_file:
            csvWriter = csv.writer(output_file, delimiter=";")
            for row in csvReader:
                find_regex = r"(https.*)"
                try:
                    result = re.search(find_regex, row[2])
                    replace_str = unwrap_url(result.group())
                    # substitute
                    new_csv_str = re.sub(find_regex, replace_str, row[2])
                    row[2] = new_csv_str
                    csvWriter.writerow(row)
                except Exception:
                    csvWriter.writerow(row)


if __name__ == "__main__":
    # specifying the path to csv files
    path = sys.argv[1]

    # csv files in the path
    files = glob.glob(path + "/**/*.csv", recursive=True) + glob.glob(
        path + "/**/*.txt", recursive=True
    )

    p = Pool(16)
    p.map(process_file, files)
