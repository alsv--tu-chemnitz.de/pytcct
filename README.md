# PyTCCT

A simple example script for corpus collection from Twitter using [Tweepy](https://github.com/tweepy).

## Installation
Running this on Linux (possibly inside screen or tmux) is the intended use case. For your convenience a standalone package for Windows is provided. 
### Linux
1. Clone the repository or download the source archive.
2. Install dependencies via `pip install -r requirements.txt`. You might want to use a virtualenv for this.
### MacOS
Same as Linux. You might want to consider using Python form Homebrew, as the version Apple ships is usually quite old.
Note that this is not officially supported as I have no way to test it.
### Windows
Either get Python running manually or Download the Windows Package from the [Releases Page](https://gitlab.hrz.tu-chemnitz.de/alsv--tu-chemnitz.de/pytcct/-/releases).

## Usage
### Configuration
1. Copy and rename the `settings.ini.example` file to `settings.ini`
2. You need to get your own Twitter API keys. This avoids hitting rate limits. 
    1. Create a [Twitter Application](https://apps.twitter.com).
    2. Paste the API keys into the config file. 
    For further details see steps 1-5 [this guide](https://www.slickremix.com/docs/how-to-get-api-keys-and-tokens-for-twitter/)
3. Decide whether you want to filter by location, by keywords (default) or by both (not recommended).
    1. If you want to filter by location, add a `#` in front of `filename` in the `settings.ini` file and remove the `#` in front of `bounding_box`.
    2. If you want to filter by keywords, add a `#` in front of `bounding_box` in the `settings.ini` file and remove the `#` in front of `filename`.
    3. If you want to filter by keywords and location, remove the `#` in front of `bounding_box` and `filename`. Note that these filtering options are OR connected, that means tweets matching either criteria will be returned, not just tweets matching both criteria.
    4. If you want to enable the optional language filtering, remove the `#` in front of `tweet_language` and set the language accordingly. 

### Running
Under Linux and MacOS, open a shell, optionally activate your virtualenv and run the tool via `python pytcct.py`.

Under Windows, double click the `pytcct.exe` file or run it from a shell, similar to Linux and MacOS.

### Stopping
Hit Crtl+c to abort the tool. Don't mind the Python error messages that you'll see.

## Data Format
Data will be stored in CSV files, which can conveniently be imported into a spreadsheet program. 

The data column names are: timestamp, TweetID, TweetText, TweetGeo.

For further details on the data format, refer to the [Twitter API documentation](https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/tweet).

## Limitations
This tool uses the publically available Streaming API that only contains a sample of the complete firehose API. Twitter does not provide any information on how this sample is selected. 

Also note that the filtering by geolocation includes tweets that are geo tagged and tweets based on the location that users can provide in their profile. If the last column in the CSV file contains data, the tweet was geo tagged. 